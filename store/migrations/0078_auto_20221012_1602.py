# Generated by Django 3.2.16 on 2022-10-12 16:02

from django.db import migrations

def full_name(fullname):
    if not fullname:
        person = ""
    else:
        person = fullname.split(" et ")[0]
        person = person.split(",")[0]
    try:
        first_name, last_name = person.split(" ", maxsplit=1)
    except ValueError:
        first_name = ""
        last_name = ""
    return first_name.title(), last_name.title()


def define_relations_to_persons(apps, schema_editor):
    Library = apps.get_model('store', 'Library')
    Lab = apps.get_model('store', 'Lab')
    Person = apps.get_model('store', 'Person')

    for library in Library.objects.all():
        # Librarian
        first_name, last_name = full_name(library.bibliothecaire)
        person, _ = Person.objects.get_or_create(email=library.email_librarian.lower(), defaults={"first_name":first_name, "last_name":last_name})
        library.librarian = person

        # Reporter
        reporter, _ = Person.objects.get_or_create(email=library.email_reporter.lower())
        library.reporter = reporter

        # Scientific Manager
        first_name, last_name = full_name(library.resp_scient)
        manager, _ = Person.objects.get_or_create(email=library.email_scientific_manager.lower(), defaults={"first_name":first_name, "last_name":last_name})
        library.scientific_manager = manager
        library.save()

    for lab in Lab.objects.all():
        manager, _ = Person.objects.get_or_create(email=lab.email_manager.lower(), defaults={"first_name":lab.managaer_first_name.title(), "last_name":lab.manager_last_name.title()})
        lab.manager = manager
        lab.save()

    Person.objects.filter(first_name="", last_name="", email="").delete()


def reverse(apps, schema_editor):
    Person = apps.get_model('store', 'Person')
    Person.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0077_auto_20221012_1602'),
    ]

    operations = [
        migrations.RunPython(define_relations_to_persons, reverse),
    ]
