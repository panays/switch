# Generated by Django 3.2.16 on 2022-10-10 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0039_auto_20221010_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lab',
            name='no_unite',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
