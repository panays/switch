# Generated by Django 3.2.16 on 2022-10-10 15:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0028_auto_20221010_1513'),
    ]

    operations = [
        migrations.RenameField(
            model_name='library',
            old_name='url_peb',
            new_name='url_loans',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='url_per',
            new_name='url_periodicals',
        ),
    ]
