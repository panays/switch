# Generated by Django 3.2.16 on 2022-10-12 14:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0068_auto_20221012_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='library',
            name='labo',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.lab'),
        ),
    ]
