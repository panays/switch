# Generated by Django 3.2.16 on 2022-10-12 11:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0060_rename_mel_peb_library_email_loan_manager'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lab',
            name='fax',
        ),
        migrations.RemoveField(
            model_name='library',
            name='fax',
        ),
        migrations.RemoveField(
            model_name='library',
            name='tel_pret',
        ),
    ]
