# Generated by Django 3.2.16 on 2022-10-12 13:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0067_auto_20221012_1345'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lab',
            name='autre_resp',
        ),
        migrations.RemoveField(
            model_name='lab',
            name='coco',
        ),
        migrations.RemoveField(
            model_name='lab',
            name='email_coco',
        ),
        migrations.RemoveField(
            model_name='lab',
            name='ldap',
        ),
    ]
