# Generated by Django 3.2.16 on 2022-10-10 15:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0029_auto_20221010_1517'),
    ]

    operations = [
        migrations.RenameField(
            model_name='library',
            old_name='url_ouvr',
            new_name='url_book_index',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='url_periodicals',
            new_name='url_periodical_index',
        ),
    ]
