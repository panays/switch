# Generated by Django 3.2.16 on 2022-10-10 15:58

from django.db import migrations


def assign_lab_types(apps, schema_editor):
    Lab = apps.get_model('store', 'Lab')
    LabType = apps.get_model('store', 'LabType')

    for lab in Lab.objects.exclude(type_unite__in=[None, ""]):
        try:
            lab.lab_type = LabType.objects.get(name=lab.type_unite)
        except:
            pass
        lab.save()


def do_nothing(apps, schema_editor):
    """
    Just here to make possible migration rollback
    """
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0036_lab_lab_type'),
    ]

    operations = [
        migrations.RunPython(assign_lab_types, do_nothing),
    ]
