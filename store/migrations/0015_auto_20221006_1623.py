# Generated by Django 3.2.16 on 2022-10-06 16:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0014_rename_date_fin_lab_closed'),
    ]

    operations = [
        migrations.RenameField(
            model_name='library',
            old_name='adr',
            new_name='address',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='ville',
            new_name='city',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='dept',
            new_name='department',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='bp',
            new_name='postal_box',
        ),
        migrations.RenameField(
            model_name='library',
            old_name='codepostal',
            new_name='postal_code',
        ),
    ]
