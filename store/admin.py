from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline

from .models import Address
from .models import Library
from .models import Structure
from .models import Labofed
from .models import LabType
from .models import Lab
from .models import Person


class AddressInline(GenericStackedInline):
    model = Address
    extra = 0
    max_num = 1


class PersonAdmin(admin.ModelAdmin):
    search_fields = ['first_name', "last_name", "email"]


class LibraryAdmin(admin.ModelAdmin):
    search_fields = ['name', "alias"]
    list_display = ["alias", "scientific_manager", "reporter"]
    autocomplete_fields = ["lab", "structures", "librarians", "scientific_manager", "reporter"]
    formfield_overrides = {
        PhoneNumberField: {'widget': PhoneNumberPrefixWidget},
    }
    inlines = [AddressInline,]

    def has_delete_permission(self, request, obj=None):
        return False


class LabAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ["name", "manager"]
    autocomplete_fields = ["structures", "manager"]
    formfield_overrides = {
        PhoneNumberField: {'widget': PhoneNumberPrefixWidget},
    }
    inlines = [AddressInline,]

    def has_delete_permission(self, request, obj=None):
        return False


class LabTypeAdmin(admin.ModelAdmin):
    search_fields = ['name']

    def has_delete_permission(self, request, obj=None):
        return False


class StructureAdmin(admin.ModelAdmin):
    search_fields = ['name']

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Library, LibraryAdmin)
admin.site.register(Lab, LabAdmin)
admin.site.register(Structure, StructureAdmin)
admin.site.register(Labofed)
admin.site.register(LabType, LabTypeAdmin)
admin.site.register(Person, PersonAdmin)
