from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType

from phonenumber_field.modelfields import PhoneNumberField

from datetime import date


class Structure(models.Model):
    name = models.CharField(max_length=255, verbose_name="nom")
    alias = models.CharField(max_length=255, blank=True, default="")
    url = models.URLField(max_length=255, blank=True, default="")

    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=254, blank=True, default="", verbose_name="prénom")
    last_name = models.CharField(max_length=254, blank=True, default="", verbose_name="nom")
    email = models.EmailField(blank=True, null=True, unique=True)

    class Meta:
        verbose_name = "personne"

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.email}"


class Address(models.Model):
    address = models.TextField(blank=True, default="", verbose_name="adresse")
    city = models.CharField(max_length=255, verbose_name="ville")
    postal_code = models.CharField(max_length=10, verbose_name="code postal")
    cedex = models.CharField(max_length=255, blank=True, default="", verbose_name="cedex")
    postal_box = models.CharField(max_length=255, blank=True, default="", verbose_name="boîte postale")
    latitude = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = "adresse"

    def __str__(self):
        return str(self.content_object)


class Library(models.Model):
    name = models.CharField(max_length=255, verbose_name="nom")
    alias = models.CharField(max_length=255)

    rcr = models.IntegerField(blank=True, null=True, unique=True)
    rnbm = models.BooleanField(default=False)
    sudoc = models.BooleanField(default=False)
    z3950 = models.BooleanField(default=False)
    pcmath = models.BooleanField(default=False)

    structures = models.ManyToManyField(Structure)
    department = models.CharField(max_length=255, blank=True, default="", verbose_name="département")
    domain = models.TextField(blank=True, default="", verbose_name="domaine")
    description = models.TextField(blank=True, default="", verbose_name="description")

    addresses = GenericRelation(Address, verbose_name="adresses")

    phone = PhoneNumberField(blank=True, verbose_name="téléphone")

    email = models.EmailField(blank=True, default="", verbose_name="mail")
    librarians = models.ManyToManyField("Person", blank=True, verbose_name="bibliothécaires", related_name="librarian_of")
    reporter = models.ForeignKey("Person", blank=True, null=True, on_delete=models.SET_NULL, verbose_name="correspondant.e", related_name="reporter_of")
    scientific_manager = models.ForeignKey("Person", blank=True, null=True, on_delete=models.SET_NULL, verbose_name="responsable scientifique", related_name="scientific_manager_of")

    url = models.URLField(max_length=255, blank=True, default="")
    url_book_index = models.URLField(max_length=255, blank=True, default="", verbose_name="url des ouvrages")
    url_periodical_index = models.URLField(max_length=255, blank=True, default="", verbose_name="url des revues")
    url_catalog_index = models.URLField(max_length=255, blank=True, default="", verbose_name="url du catalogue")
    url_news = models.URLField(max_length=255, blank=True, default="", verbose_name="url des nouvelles acquisitions")

    notes = models.TextField(blank=True, default="", verbose_name="notes")
    openning = models.TextField(blank=True, default="", verbose_name="horaires")
    closing = models.TextField(blank=True, default="", verbose_name="fermeture")
    access_conditions = models.TextField(blank=True, default="", verbose_name="conditions d'accès")
    loan_conditions = models.TextField(blank=True, default="", verbose_name="conditions de prêt")
    peb_conditions = models.TextField(blank=True, default="", verbose_name="conditions des peb")
    use_conditions = models.TextField(blank=True, default="", verbose_name="conditions d'utilisation")

    bib_section = models.IntegerField(blank=True, null=True)
    lab = models.OneToOneField("Lab", blank=True, null=True, on_delete=models.CASCADE, verbose_name="laboratoire")

    last_updated = models.DateTimeField(auto_now=True, verbose_name="dernière mise à jour")

    class Meta:
        verbose_name = "bibliothèque"

    def __str__(self):
        return self.alias


class Labofed(models.Model):
    clefed = models.IntegerField()
    clelab = models.IntegerField()

    def __str__(self):
        return str(self.pk)


class LabType(models.Model):
    name = models.CharField(max_length=50, unique=True, verbose_name="nom")

    class Meta:
        verbose_name = "type d'unité"
        verbose_name_plural = "types d'unités"

    def __str__(self):
        return self.name


class Lab(models.Model):
    name = models.CharField(max_length=255, verbose_name="nom")
    alias = models.CharField(max_length=255, blank=True, default="")

    unit_type = models.ForeignKey("LabType", blank=True, null=True, on_delete=models.CASCADE, verbose_name="type d'unité")
    unit_number = models.IntegerField(blank=True, null=True, verbose_name="numéro d'unité")

    structures = models.ManyToManyField(Structure, verbose_name="tutelles")

    addresses = GenericRelation(Address, verbose_name="adresses")
    phone = PhoneNumberField(blank=True, verbose_name="téléphone")
    url = models.URLField(max_length=255, blank=True, default="")
    email = models.EmailField(blank=True, default="", verbose_name="mail")

    manager = models.ForeignKey("Person", blank=True, null=True, on_delete=models.SET_NULL, verbose_name="responsable")

    created = models.DateField(auto_now_add=True, blank=True, null=True, verbose_name="créé le")
    closed = models.DateField(blank=True, null=True, verbose_name="fermé le")
    last_updated = models.DateTimeField(auto_now=True, verbose_name="dernière mise à jour")

    visible = models.BooleanField(default=True, verbose_name="visible")

    class Meta:
        verbose_name = "laboratoire"

    def __str__(self):
        return self.name
